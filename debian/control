Source: admesh
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Anton Gladky <gladk@debian.org>
Section: math
Priority: optional
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/science-team/admesh
Vcs-Git: https://salsa.debian.org/science-team/admesh.git
Homepage: https://github.com/admesh/admesh

Package: admesh
Architecture: any
Depends: libadmesh1 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Suggests: geomview,
          gmsh,
          paraview,
          python3-admesh
Description: Tool for processing triangulated solid meshes. Binary
 Currently, ADMesh only reads the STL file
 format that is used for rapid prototyping applications,
 although it can write STL, VRML, OFF, and DXF files.
 Some features of admesh are: Fill holes in the mesh
 by adding facets. Repair facets by connecting
 nearby facets. Repair normal directions
 (i.e. facets should be CCW) Remove degenerate
 facets (i.e. facets with 2 or more vertices equal)

Package: libadmesh-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: libadmesh1 (= ${binary:Version}),
         pkg-config,
         ${misc:Depends},
         ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Tool for processing triangulated solid meshes. Development files
 Currently, ADMesh only reads the STL file
 format that is used for rapid prototyping applications,
 although it can write STL, VRML, OFF, and DXF files.
 Some features of admesh are: Fill holes in the mesh
 by adding facets. Repair facets by connecting
 nearby facets. Repair normal directions
 (i.e. facets should be CCW) Remove degenerate
 facets (i.e. facets with 2 or more vertices equal)
 .
 The package contains development files.

Package: libadmesh1
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${misc:Depends},
         ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Tool for processing triangulated solid meshes. Shared library
 Currently, ADMesh only reads the STL file
 format that is used for rapid prototyping applications,
 although it can write STL, VRML, OFF, and DXF files.
 Some features of admesh are: Fill holes in the mesh
 by adding facets. Repair facets by connecting
 nearby facets. Repair normal directions
 (i.e. facets should be CCW) Remove degenerate
 facets (i.e. facets with 2 or more vertices equal)
 .
 The package contains shared library.
